package com.example.diegoromano.fitnessandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends Activity {


    private TextView textView;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        textView = (TextView) findViewById(R.id.textView);
        imageView = (ImageView ) findViewById(R.id.imageView);
        Animation myAnimation = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        textView.startAnimation(myAnimation);
        imageView.startAnimation(myAnimation);

        final Intent intent = new Intent(this,MainActivity.class);

        Thread timer = new Thread(){
            public void run(){
                try{

                    sleep(5000);

                }catch (InterruptedException e){

                    e.printStackTrace();

                }finally {

                    startActivity(intent);
                    finish();

                }
            }
        };

        timer.start();
    }
}
